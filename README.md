laravelbnb
---
1.composer create-project --prefer-dist laravel/laravel laravelbnb "6.*"
2.cd laravelbnb
3.composer install
4.composer require laravel/ui:^1.0 --dev
5.php artisan ui vue
6.npm install vue-router@3
7.npm install
8.npm run watch
9.php artisan serve